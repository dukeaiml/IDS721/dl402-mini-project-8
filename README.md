# IDS 721 Mini Project 8
This project demonstrates the process of working with Rust functions in a command line tool. Also, it involves some automated tests for the project's functionality. The main function is that the command line will take a string as input and reverse it for output. 

## Build Process:
Follow these steps to set up and run your project:

### Step 1: Project Setup

1. **Create a New Rust Project:**
   Initialize a new Rust project using Cargo.
   
   ```
   cargo new <PROJECT_NAME> --bin
   ```
2. **Add Functionalities:**
    Add the code for ingesting data and query data in the main.rs file.

### Step 2: Tool Functionality

#### Running the program: 
After implementing the program, we can check the functionality of the tool. Run the program using the following command:

```
cargo run -- "<String you want to reverse>"
```

The sample input and output are like this: 
![Alt text](/screenshots/output.png)

#### Testing the program: 
We can add automateed tests in the code that looks like this 

```
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_process_data() {
        assert_eq!(process_data("rust"), "tsur");
        assert_eq!(process_data("hello"), "olleh");
    }
}
```

In this test function, we can see that it involves two seperated cases, which the inputs are "rust" and "hello". We can then run the command 

```
cargo test
```

to see whether the program passes the test and it is shown in the following screenshot: 
![Alt text](/screenshots/test.png)

We can see from here that the test reports "ok", and 1 tests passed and no test failed. 